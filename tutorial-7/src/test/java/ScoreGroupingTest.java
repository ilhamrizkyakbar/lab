import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScoreGroupingTest {

    private Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        scores.put("Alpha", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void groupByScores() {
        assertEquals("{11=[Charlie, Foxtrot], 12=[Alpha], 15=[Emi, Bob, Delta]}",
                ScoreGrouping.groupByScores(scores).toString());
    }
}
