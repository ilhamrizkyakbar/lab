package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {
        if(name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", "Hello, " + name + "! I hope you interested to hire me");
        }
        StringBuilder buatCv = new StringBuilder();
        buatCv.append("Name: Ilham Rizky Akbar\n");
        buatCv.append("Birthdate: 01/07/1998\n");
        buatCv.append("Birthplace: Jakarta\n");
        buatCv.append("Education History\n");
        buatCv.append("-- SD Al Azhar Jakapermai --\n");
        buatCv.append("-- SMP Labschool Jakarta --\n");
        buatCv.append("-- SMA 81 Jakarta --\n");
        buatCv.append("-- Faculty of Computer Science --\n");
        model.addAttribute("cv", buatCv.toString());

        StringBuilder desc = new StringBuilder();
        desc.append("I'm on my second-year at CSUI.\n");
        desc.append("I've alot of interest in comic and some programming.\n");
        desc.append("Currently, I looking for some intern programs.");
        model.addAttribute("description", desc.toString());

        return "greeting";
    }

}
