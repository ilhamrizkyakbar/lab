package id.ac.ui.cs.advprog.tutorial3.composite;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main {
	public static void main(String args[]) {
		Company company = new Company();
		Ceo ceo = new Ceo("David", 250000);
		Cto cto = new Cto("Johnny", 100000);
		FrontendProgrammer fp = new FrontendProgrammer("Emmanuel",35900);
		BackendProgrammer bp = new BackendProgrammer("King",21280);
		NetworkExpert ne = new NetworkExpert("Ben",50000);
		SecurityExpert se = new SecurityExpert("Wayne",74900);
		UiUxDesigner uud = new UiUxDesigner("James", 99999);
		
		company.addEmployee(ceo);
		company.addEmployee(cto);
		company.addEmployee(fp);
		company.addEmployee(bp);
		company.addEmployee(ne);
		company.addEmployee(se);
		company.addEmployee(uud);
		
		for(int i=0; i<company.employeesList.size(); i++) {
			System.out.println("Nama: " + company.employeesList.get(i).getName()+ " - Role: " + company.employeesList.get(i).getRole() + " - Gaji: "+ company.getAllEmployees().get(i).getSalary());
		}
		
		System.out.println("Total Gaji Perusahaan: " + company.getNetSalaries());
		
	}

}
