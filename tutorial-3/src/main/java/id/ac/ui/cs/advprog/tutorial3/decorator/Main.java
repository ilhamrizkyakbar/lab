package id.ac.ui.cs.advprog.tutorial3.decorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main {
	public static void main(String args[]) {
		ThickBunBurger thick = new ThickBunBurger();
		Cheese cheese = new Cheese(thick);
		System.out.println(cheese.getDescription());
		System.out.println(cheese.cost());
		ThinBunBurger thiny = new ThinBunBurger();
		BeefMeat beef = new BeefMeat(thiny);
		Cucumber cuc = new Cucumber(thiny);
		System.out.println(cuc.cost());
		System.out.println(cuc.getDescription());
		System.out.println(beef.getDescription());
		System.out.println(beef.cost());
	}
}
