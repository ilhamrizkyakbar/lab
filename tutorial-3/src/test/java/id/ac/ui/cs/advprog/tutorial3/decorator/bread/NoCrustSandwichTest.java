package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;
public class NoCrustSandwichTest {
    private NoCrustSandwich noCrustSandwich;

    @Before
    public void setUp() {
        noCrustSandwich = new NoCrustSandwich();
    }

    @Test
    public void testMethodCost() {
        assertEquals(2.00, noCrustSandwich.cost(), 0.00);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("No Crust Sandwich", noCrustSandwich.getDescription());
    }
}
