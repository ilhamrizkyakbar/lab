package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
	
	private PizzaStore pizzaStore;
	
	@Before
	public void setUp() throws Exception {
		pizzaStore = new DepokPizzaStore();
    }
	
	@Test
	public void testCreateCheesePizza(){
		Pizza pizza = pizzaStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
		assertNotNull(pizza);
	}
	
	@Test
    public void testCreateVeggiesPizza() {
        Pizza pizza = pizzaStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
		assertNotNull(pizza);
    }
	
	@Test
    public void testCreateClamPizza() {
        Pizza pizza = pizzaStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
        assertNotNull(pizza);
    }

}
