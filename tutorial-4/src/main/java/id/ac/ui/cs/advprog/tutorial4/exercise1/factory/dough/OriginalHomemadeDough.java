package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class OriginalHomemadeDough implements Dough {
    public String toString() {
        return "Our store original dough";
    }

}
