package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class SpicyBbqSauce implements Sauce {
    public String toString() {
        return "Spicy BBQ Sauce";
    }

}
