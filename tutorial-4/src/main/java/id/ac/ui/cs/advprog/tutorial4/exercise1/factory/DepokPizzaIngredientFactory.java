package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BlueCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.RazorClam;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.OriginalHomemadeDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SpicyBbqSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Carrot;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory{
	public Dough createDough() {
        return new OriginalHomemadeDough();
    }

    public Sauce createSauce() {
        return new SpicyBbqSauce();
    }

    public Cheese createCheese() {
        return new BlueCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Garlic(), new Onion(), new Mushroom(), new Carrot(), new Spinach()};
        return veggies;
    }

    public Clams createClam() {
        return new RazorClam();
    }

}
