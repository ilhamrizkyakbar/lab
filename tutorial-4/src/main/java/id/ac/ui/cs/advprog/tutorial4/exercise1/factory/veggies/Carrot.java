package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Carrot implements Veggies {

    public String toString() {
        return "Shredded Carrot";
    }

}
