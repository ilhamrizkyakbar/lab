package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RazorClam implements Clams {

    public String toString() {
        return "Razor Clams from The Pacific";
    }
}
